package ardhyanti.rizia.app04_rizia

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{

    val RC_PERKALIAN_SUKSES : Int = 100

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnGallery ->{
                var intentGal = Intent()
                intentGal.setType("image/*")
                intentGal.setAction(Intent.ACTION_GET_CONTENT)
                startActivity(Intent.createChooser(intentGal, "Pilih Gambar ..."))
            }
            R.id.btnDetik ->{
                var webUri = "https://detik.com"
                var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))  //ACTION VIEW UNTUK MENAMPILKAN HALAMAN WEB
                startActivity(intentInternet)
            }
            R.id.btnPerkalian ->{
                var intent = Intent(this, PerkalianActivity::class.java)
                intent.putExtra("X",edHasil.text.toString())
                startActivityForResult(intent,RC_PERKALIAN_SUKSES)

            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == RC_PERKALIAN_SUKSES)
                edHasil.setText(data?.extras?.getString("hasilkali"))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnDetik.setOnClickListener(this)
        btnGallery.setOnClickListener(this)
        btnPerkalian.setOnClickListener(this)

    }
}
