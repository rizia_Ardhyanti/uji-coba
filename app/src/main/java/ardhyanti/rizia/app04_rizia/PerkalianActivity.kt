package ardhyanti.rizia.app04_rizia

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_perkalian.*
import java.text.DecimalFormat

class PerkalianActivity : AppCompatActivity(), View.OnClickListener{

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = edX.text.toString().toDouble()
        y = edY.text.toString().toDouble()
        hasil = x*y
        txHasilKali.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perkalian)
        btnKali.setOnClickListener(this)

        var paket : Bundle? = intent.extras
        edX.setText(paket?.getString("X"))
    }

    override fun finish() {
        var intent = Intent()

        intent.putExtra("hasilkali",txHasilKali.text.toString())
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }
}